/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class AutoShop { // zad7

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Car car = new Car(120.0, 8000.0, "red");
        Truck carTruck = new Truck(3500.0, 100.0, 40000.0, "blue");
        Hatchback carHatckback = new Hatchback(2008, 0.15, 200.0, 30000.0, "black");
        Sedan carSedan = new Sedan(4000.0, 180.0, 20000.0, "red");
        
        System.out.println("Car: " + car.getSalePrice());
        System.out.println("Truck: " + carTruck.getSalePrice());
        System.out.println("Hatchback: " + carHatckback.getSalePrice());
        System.out.println("Sedan: " + carSedan.getSalePrice());
    }
    
}
