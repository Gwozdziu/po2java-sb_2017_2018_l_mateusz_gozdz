
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class SavingsAccount {
    private static double annuallInterestRate = 0.03;
    private double savingsBalance;

    public SavingsAccount() {
        savingsBalance = 0.0;
    }

    public SavingsAccount(double savingsBalance) {
        this.savingsBalance = savingsBalance;
    }

    public double getSavingsBalance() {
        return savingsBalance;
    }

    public static double getAnnuallInterestRate() {
        return annuallInterestRate;
    }
    
    
    
    public double moneyPayment(double out) {//wypłata pieniędzy, zwraca saldo
        if(out > 0 && savingsBalance >= out) {
            savingsBalance -= out;
        }
        else try {
            throw new negativeValueException();
        } catch (negativeValueException ex) {
            System.out.println(ex.getMessage());
        }
        return savingsBalance;
    }
    public double depositMoney(double in) {
        if(in > 0) {
            savingsBalance += in;
        }
        else try {
            throw new negativeValueException();
        } catch (negativeValueException ex) {
            System.out.println(ex.getMessage());
        }
        return savingsBalance;
    } 
    public void calclulateMonthlyInterest() {
        savingsBalance += annuallInterestRate * savingsBalance / 12;
    }
    public static void  modifyInterestRate(double newRate) {
        annuallInterestRate = newRate;
    }

    @Override
    public String toString() {
        return "Saldo konta " + savingsBalance;//To change body of generated methods, choose Tools | Templates.
    }
    
}
class negativeValueException extends Exception {
    public negativeValueException() {
        super("Nie można wpłacać/wypłacać ujemnej liczby!");
    }
}
