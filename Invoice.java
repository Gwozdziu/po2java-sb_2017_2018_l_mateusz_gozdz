/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class Invoice {
    private int ID;
    private String itemDescription;
    private int quantityOfPurchasedItems;
    private double pricePerItem;

    public Invoice(int ID, String itemDescription, int quantityOfPurchasedItems, double pricePerItem) {
        this.ID = ID;
        this.itemDescription = itemDescription;
        this.quantityOfPurchasedItems = quantityOfPurchasedItems;
        this.pricePerItem = pricePerItem;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public int getQuantityOfPurchasedItems() {
        return quantityOfPurchasedItems;
    }

    public void setQuantityOfPurchasedItems(int quantityOfPurchasedItems) {
        this.quantityOfPurchasedItems = quantityOfPurchasedItems;
    }

    public double getPricePerItem() {
        return pricePerItem;
    }

    public void setPricePerItem(double pricePerItem) {
        this.pricePerItem = pricePerItem;
    }
    
    public double getInvoiceAmount() {
        return this.pricePerItem * this.quantityOfPurchasedItems;
    }

    @Override
    public String toString() {
        return "Faktura na przedmiot " + this.itemDescription + " ID: " + this.ID + "\t| Ilość: " + this.quantityOfPurchasedItems + "\tCena za sztuke: " + this.pricePerItem + "\t| Cena faktury: " + this.getInvoiceAmount();//To change body of generated methods, choose Tools | Templates.
    }
}
