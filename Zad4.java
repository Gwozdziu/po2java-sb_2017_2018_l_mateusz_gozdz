
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class Zad4 {
    public static void main(String[] args){
        ArrayList<Invoice> listOfInvoices = new ArrayList<>();
        listOfInvoices.add(new Invoice(1337, "Nakrętka", 300, 0.12));
        listOfInvoices.add(new Invoice(2115, "Śruba", 300, 0.3));
        listOfInvoices.add(new Invoice(333,  "Gwóźdź", 450, 0.15));
        listOfInvoices.add(new Invoice(213,  "Wiertarka", 2, 399.99));
        listOfInvoices.add(new Invoice(123,  "Podkładka", 300, 0.1));
        
        double costOfInvoices = 0;
        for(Invoice item:listOfInvoices) {
            System.out.println(item);
            costOfInvoices += item.getInvoiceAmount();
        }
        System.out.println("Całkowity koszt faktur: " + costOfInvoices);
    }
}
