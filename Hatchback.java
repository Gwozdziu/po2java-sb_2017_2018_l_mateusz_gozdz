/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class Hatchback extends Car{
    private final int year;
    private final double manufacturerDiscount;

    public Hatchback(int year, double manufacturerDiscount, double speed, double price, String color) {
        super(speed, price, color);
        this.year = year;
        this.manufacturerDiscount = manufacturerDiscount;
    }
    
    @Override
    public double getSalePrice() {
        return getPrice() - getPrice() * manufacturerDiscount;
    }
}
