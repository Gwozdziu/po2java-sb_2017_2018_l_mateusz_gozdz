import java.util.*;
import java.io.*;

public class Zad5 {

	public static void main(String[] args) {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);
			Vector<Double> sum = null;
			boolean w = true;
			while(w) {
				w = false;
				System.out.print("Podaj pierwszy wektor: ");
				Vector<Double> A = readVec(br.readLine());
				System.out.print("Podaj drugi wektor: ");
				Vector<Double> B = readVec(br.readLine());
				try {
					sum = addVec(A, B);
				} catch(DifferentVectorSizeException e) {
					System.out.println("Błąd: " + e.getMessage() + " | Pierwszy ma długość: " + e.lenA + ", a drugi: " + e.lenB);
					System.out.println("Podaj ponownie wektory.");
					w = true;
				}
			}
			saveVec(sum, "sum.txt");
			br.close();
			isr.close();
		} catch(IOException e) {
			System.out.println("Error IO :( " + e.getLocalizedMessage());
		}
	}
	
	static Vector<Double> readVec(String l) {
		Scanner scan = new Scanner(l);
		Vector<Double> vec = new Vector<Double>();
		while(scan.hasNext()) {
			if(scan.hasNextDouble())
				vec.add(scan.nextDouble());
			else
				scan.next();
		}
		scan.close();
		return vec;
	}
	
	static Vector<Double> addVec(Vector<Double> A, Vector<Double> B) throws DifferentVectorSizeException {
		if(A.size() != B.size())
			throw new DifferentVectorSizeException(A.size(), B.size());
		Vector<Double> sum = new Vector<Double>(A.size());
		for(int i = 0; i < A.size(); ++i)
			sum.add(A.elementAt(i) + B.elementAt(i));
		return sum;
	}
	
	static void saveVec(Vector<Double> v, String f) throws IOException {
		OutputStream out = new FileOutputStream(f);
		Writer wr = new OutputStreamWriter(out);
		PrintWriter pr = new PrintWriter(wr);
		for(int i = 0; i < v.size(); ++i)
			pr.print(v.elementAt(i) + " ");
		pr.close();
		wr.close();
		out.close();
	}
}
