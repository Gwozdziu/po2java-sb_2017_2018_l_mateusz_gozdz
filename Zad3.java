
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class Zad3 {
    public static void main(String[] args){
        if(args.length < 1)
        {
            System.out.println("Brak podanej nazwy pliku");
            System.exit(0);
        }
        int a, b, c;
        
        System.out.println("Podaj wspolczynnik A:");
        a = getNumber();
        System.out.println("Podaj wspolczynnik B:");
        b = getNumber();
        System.out.println("Podaj wspolczynnik C:");
        c = getNumber();
        String result = quadraticFormula(a, b, c);
        System.out.println(result);
        saveResult(args[0], result);
        
    }
    public static int getNumber(){
        Scanner scan = new Scanner(System.in);
        int returnNumber = 0;
        while (true) {
            try {
                returnNumber = Integer.parseInt(scan.next());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Musisz podac liczbe w formacie int");
            }
        }
        return returnNumber;
    }
    public static String quadraticFormula(int a, int b, int c){
        if (a == 0) {
            return "To nie jest rwnanie kwadratowe";
	}
        
	int d = b * b - 4 * a * c;
        if (d < 0) {
            return "Brak pierwiastkw rzeczywistych";
        } else if (d == 0) {
            int x = -b / (2 * a);
            return "Jeden pierwiastek: " + x;
        } else {
            double x1 = (-b - Math.sqrt(d)) / (2 * a);
            double x2 = (-b + Math.sqrt(d)) / (2 * a);
            return "Dwa pierwiastki: " + x1 + " i " + x2;
        }
    }
    public static void saveResult(String fileName, String result){
        try {
            PrintWriter out = new PrintWriter(fileName+".txt");
            out.println(result);
			out.close();
        } catch (FileNotFoundException e) {
            System.out.println("Nie udalo sie zapisac pliku");
        }
    }
}
