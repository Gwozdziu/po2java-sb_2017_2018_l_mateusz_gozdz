package commands;

import java.util.ArrayList;
import sql.Types;
import myexceptions.WrongTypeOfColumnException;
import sql.Database;
import sql.Table;

public class CommandCreate{
    public static ArrayList<String> colName(ArrayList<String> command) {
        ArrayList<String> nameOfColumns = new ArrayList<>();
        for(int i = 3; i < command.size(); i+=2){
            nameOfColumns.add(command.get(i));
        }
        return nameOfColumns;
    }
    public static ArrayList<String> colType(ArrayList<String> command) throws Exception{
        ArrayList<String> nameOfColumns = new ArrayList<>();
        for(int i = 4; i< command.size(); i+=2){
            if (Types.checkName(command.get(i))) nameOfColumns.add(command.get(i));
            else throw new WrongTypeOfColumnException("Z�a nazwa kolumny!",command.get(i));
        }
        return nameOfColumns;
    }
    public static void makeTable(ArrayList<String> command, ArrayList<Database> data, Integer LastData)
    throws Exception{
        ArrayList<String> nameOfColumns = CommandCreate.colName(command);
        ArrayList<String> typeOfColumns = CommandCreate.colType(command);
        Table TABLE = Table.Create(nameOfColumns, typeOfColumns, command.get(2));
        System.out.println("Nazwa bazy do kt�rej wstawiamy: " + data.get(LastData));
        data.get(LastData).addToDatabase(TABLE);
    }
}
