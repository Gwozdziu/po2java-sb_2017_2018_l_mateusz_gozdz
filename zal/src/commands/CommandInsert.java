package commands;

import sql.Database;
import java.util.ArrayList;
import sql.Table;
import myexceptions.EmptyDatabaseException;
import myexceptions.TableDoesNotExistsException;

public class CommandInsert {
  public static ArrayList<String> insertCols(ArrayList<String> command){
    ArrayList<String> columns = new ArrayList<>();
    int i = 3;
    for(; i < command.size(); i++){
      if(command.get(i).equalsIgnoreCase("VALUES")){
        command.set(i, command.get(i).toUpperCase());
        break;
      } else {
        columns.add(command.get(i));
      }
    }
    if(columns.isEmpty()) columns.add("*");
    return columns;
  }
  public static ArrayList<String> argInsert(ArrayList<String> command){
    ArrayList<String> arguments = new ArrayList<>();
    int j = command.indexOf("VALUES") + 1;
    for(; j < command.size(); j++)
      arguments.add(command.get(j));
    return arguments;
  }
    public static void make(ArrayList<String> command, Database ref)
    throws Exception{
        String nameOfTable = command.get(2);
        ArrayList<String> columns = CommandInsert.insertCols(command);
        ArrayList<String> arguments = CommandInsert.argInsert(command);
        ArrayList<Table> tables = ref.getTables();
        if(tables.isEmpty())
            throw new EmptyDatabaseException("Brak tabeli w tej bazie.");
        int i = 0;
        while(i<tables.size()){
            if(tables.get(i).getName().equals(nameOfTable)){
                tables.get(i).Insert(columns,arguments);
                break;
            }
            i++;
        }
        if(i==tables.size())
            throw new TableDoesNotExistsException("W bazie nie ma tablicy o podanej nazwie");
        System.out.print("Do tabeli: " + nameOfTable + " \n");
        System.out.println("Dodano: " + arguments);
    }
}
