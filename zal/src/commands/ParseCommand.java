package commands;
 import java.util.ArrayList;

public class ParseCommand{
  public static String replaceSpecialSigns(String str){
    str = str.replace(',',' ');
    str = str.replace(';',' ');
    str = str.replace('(',' ');
    str = str.replace(')',' ');
    str = str.replace('\'',' ');
    return str;
  }
  public static ArrayList<String> readCommand(String str ){
    str = replaceSpecialSigns(str);
    String[] parsed = str.split(" ");
    ArrayList<String> command = new ArrayList<String>();

    for(int i = 0; i < parsed.length ;i++){
      if(parsed[i].equals("")){
        continue;
      }
      command.add(parsed[i]);
    }
    return command;
  }
}
