package commands;
 import java.util.Scanner;
 import java.util.ArrayList;
 import myexceptions.*;
 import myio.IOclass;
 import sql.*;

public class ProjMain {
    public static void main(String[] args) throws Exception{
        try{
            Scanner read = new Scanner(System.in);
            ArrayList<Database> data = new ArrayList<>();
            Integer LastData = -1;
            String whichDatabase;
            while(true){
                try {
                	System.out.print("> ");
                    String word = read.nextLine();
                    if(word.equalsIgnoreCase("exit;")){
                            if(!data.isEmpty())IOclass.writeObjects(data.get(LastData));
                            System.out.println("Koniec pracy.");
                            System.exit(0);
                    }
                    if(word.charAt(word.length()-1) != ';')
                        throw new WithoutSemicolonException("Komendy musisz ko�czy� �rednikiem");
                    ArrayList<String> command;
                    command = ParseCommand.readCommand(word);
                    if(command.get(0).equalsIgnoreCase("Create")){
                        if(command.get(1).equalsIgnoreCase("Database")){
                            whichDatabase = command.get(2);
                            Database buff = new Database(whichDatabase);
                            data.add(buff);
                            LastData = data.indexOf(buff);
                            System.out.println("Indeks dodanej bazy: " + LastData);
                        }
                        else if(command.get(1).equalsIgnoreCase("Table")){
                            ArrayList<String> nameOfColumns = CommandCreate.colName(command);
                            ArrayList<String> typeOfColumns = CommandCreate.colType(command);
                            Table TABLE = Table.Create(nameOfColumns, typeOfColumns, command.get(2));
                            System.out.println("Dodano tabel� do bazy " + data.get(LastData));
                            data.get(LastData).addToDatabase(TABLE);
                            
                        }
                        else throw new SyntaxErrorException("Mo�esz tworzy� jedynie bazy i tabele.");
                    }
                    else if(command.get(0).equalsIgnoreCase("USE")){
                        LastData = CommandUse.make(data, command, LastData);
                    }
                    else if(command.get(0).equalsIgnoreCase("Insert")){
                        CommandInsert.make(command, data.get(LastData));
                    }
                    else if(command.get(0).equalsIgnoreCase("Select")){
                        CommandSelect.make(command, data, LastData);
                    }
                    else if(command.get(0).equalsIgnoreCase("Update")){
                        CommandUpdate.make(data, LastData, command);
                    }
                    else if(command.get(0).equalsIgnoreCase("Delete")){
                        ArrayList<Table> tables = data.get(LastData).getTables();
                        Integer whichTable = data.get(LastData).getPositionTableByName(command.get(2));
                        if(whichTable.equals(-1))
                            throw new TableDoesNotExistsException("W bazie nie ma tablicy o podanej nazwie");
                        tables.get(whichTable).Delete();
                        data.get(LastData).setTables(tables);
                        System.out.println("Usuni�to wszystkie dane z tabeli: " + command.get(2));
                    }
                    else {
                        throw new NotKnowCommandException("Nieznana komenda");
                    }
                } catch(BasicException e) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            
        }
    }
}
