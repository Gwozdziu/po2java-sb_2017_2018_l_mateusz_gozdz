package commands;

import java.util.ArrayList;
import sql.Table;
import sql.Database;
import myexceptions.SyntaxErrorException;
import myexceptions.TableDoesNotExistsException;
import myexceptions.WrongColumnException;

public class CommandUpdate {
    public static void make(ArrayList<Database> data, Integer LastData, ArrayList<String> command)
    throws Exception {
        ArrayList<Table> tables = data.get(LastData).getTables();
        if(!command.get(4).equals("="))
            throw new SyntaxErrorException("Z�a sk�adnia");
        Integer whichTable = data.get(LastData).getPositionTableByName(command.get(1));
        if(whichTable.equals(-1))
            throw new TableDoesNotExistsException("Brak tabeli o takiej nazwie");
        String whichColumn = command.get(3);
        String newValue = command.get(5);
        Integer indexWhichColumn;
        Table buff = tables.get(whichTable);
        indexWhichColumn = tables.get(whichTable).getNameOfColumns().indexOf(whichColumn);
        if(indexWhichColumn.equals(-1))
            throw new WrongColumnException("Nie ma takiej kolumny");
        buff.Update(whichColumn,newValue);
        tables.set(whichTable, buff);
        data.get(LastData).setTables(tables);
        System.out.println("Pomy�lnie zmieniono dane w kolumnie " + whichColumn);
    }
}
