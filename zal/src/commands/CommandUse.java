package commands;

import myio.IOclass;
import sql.Database;
import java.util.ArrayList;
import java.io.FileNotFoundException;

public class CommandUse {
    public static Integer make(ArrayList<Database> data, ArrayList<String> command, Integer LastData)
    throws Exception {
        try {
            Database buff = IOclass.readObjects(command.get(1));
            data.add(buff);
            LastData = data.size() - 1;
            System.out.println("U�ywasz bazy " + data.get(LastData));
        } catch(java.io.FileNotFoundException e){
            System.out.println("Brak takiej bazy.");
        } finally {
            return LastData;
        }
    }
}
