package commands;

import java.util.ArrayList;
import sql.Table;
import sql.Database;
import myexceptions.EmptyDatabaseException;
import myexceptions.TableDoesNotExistsException;

public class CommandSelect {
    public static void make(ArrayList<String> command, ArrayList<Database> data, Integer LastData) throws Exception{
        String nameOfTable = command.get(command.size()-1);
        if(data.get(LastData).getPositionTableByName(nameOfTable).equals(-1))
            throw new TableDoesNotExistsException("W tej bazie nie ma takiej tabeli.");
        ArrayList<String> nameOfColumns = new ArrayList<>();
        int j = 1;
        while(!command.get(j).equalsIgnoreCase("from")){
            nameOfColumns.add(command.get(j));
            j++;
        }
        ArrayList<Table> tables = data.get(LastData).getTables();
        if(tables.isEmpty())
            throw new EmptyDatabaseException("Baza jest pusta.");
        int i = 0;
        String select = new String();
        while(i<tables.size()){
            if(tables.get(i).getName().equals(nameOfTable)){
                select = tables.get(i).Select(nameOfColumns);
                break;
            }
            i++;
        }
        System.out.println(select);
    }
}