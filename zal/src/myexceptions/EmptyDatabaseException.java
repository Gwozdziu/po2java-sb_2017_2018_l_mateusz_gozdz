package myexceptions;

public class EmptyDatabaseException extends BasicException {
    public EmptyDatabaseException(String message) {
      super(message);
    }
}
