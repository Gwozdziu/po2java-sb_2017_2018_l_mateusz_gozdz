package myexceptions;

public class WrongColumnException extends BasicException {
    public WrongColumnException(String message) {
      super(message);
    }
}
