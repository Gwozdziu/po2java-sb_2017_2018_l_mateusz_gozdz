package myexceptions;

public class TableDoesNotExistsException extends BasicException {
    public TableDoesNotExistsException(String message) {
      super(message);
    }
}
