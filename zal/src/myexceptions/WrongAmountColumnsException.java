package myexceptions;

public class WrongAmountColumnsException extends BasicException {
    public WrongAmountColumnsException(String message) {
      super(message);
    }
}
