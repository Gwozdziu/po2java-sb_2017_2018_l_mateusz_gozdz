package myexceptions;

public class WrongAmountArgumentsException extends BasicException {
    public WrongAmountArgumentsException(String message) {
      super(message);
    }
}
