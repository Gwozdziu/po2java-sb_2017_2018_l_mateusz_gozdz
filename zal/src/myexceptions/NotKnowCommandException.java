package myexceptions;

public class NotKnowCommandException extends BasicException {
    public NotKnowCommandException(String message) {
      super(message);
    }
}
