package myio;
 import java.io.*;
 import java.util.Scanner;
 import java.util.ArrayList;
 import sql.Database;
public class IOclass {
  public static void writeObjects(Database data)
  throws Exception{
    File file = new File(data.getFileName()+".txt");
    FileOutputStream fo = new FileOutputStream(file);
    ObjectOutputStream output = new ObjectOutputStream(fo);
    output.writeObject(data);
    output.close();
    fo.close();
  }
  public static Database readObjects(String nameOfFile)
  throws Exception
  {
    File file = new File(nameOfFile+".txt");
    FileInputStream fi = new FileInputStream(file);
    ObjectInputStream input = new ObjectInputStream(fi);
    Database buff = null;
    try {
      while(true){
        buff = (Database)input.readObject();
      }
    } catch (EOFException e){

    }
    input.close();
    fi.close();
    return buff;
  }
}
