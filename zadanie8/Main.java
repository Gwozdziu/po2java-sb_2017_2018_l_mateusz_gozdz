package zadanie8;

public class Main {

	public static void main(String[] args) {
		
		Building building1 = new Building();
		Floor floor1 = new Floor(building1);
		Floor floor2 = new Floor(building1);
                Floor floor8 = new Floor(building1);
		Room room1 = new Room(building1, floor1);
		Room room2 = new Room(building1, floor1);
		Room room3 = new Room(building1, floor2);
		Room room4 = new Room(building1, floor2);
		Room room5 = new Room(building1, floor2);
		building1.description();
		floor1.description();
		floor2.description();
                floor8.description();
		room4.description();
		
		Building building2 = new Building();
		Floor floor3 = new Floor(building2);
		Floor floor4 = new Floor(building2);
		Floor floor5 = new Floor(building2);
		Room room6 = new Room(building2, floor3);
		Room room7 = new Room(building2, floor3);
		Room room8 = new Room(building2, floor4);
                
                floor3.description();
                floor4.description();
		building1.id.compareFloors(building2);
		building1.id.compareRooms(building2);
		building2.id.compareRooms(building1);
		
	}

}