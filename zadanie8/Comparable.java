package zadanie8;

public interface Comparable {
	public void compareFloors(Building building);
	public void compareRooms(Building building);
}