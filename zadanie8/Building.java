package zadanie8;

public class Building extends Location {
	private static int currentID = 1;
	public Building() {
		id.buildingID = currentID++;
	}

        @Override
	public void description() {
		System.out.println("Budynek nr: " + id.buildingID + " ma " + id.floorID.size() + " pietra i " + id.roomID.size() + " pokoi.");
	}
}