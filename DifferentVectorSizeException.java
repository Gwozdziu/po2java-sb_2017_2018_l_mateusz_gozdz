public class DifferentVectorSizeException extends Exception {
	
	public final int lenA, lenB;
	
	public DifferentVectorSizeException(int lenA, int lenB) {
		super("Wektory są różnej długości");
		this.lenA = lenA;
		this.lenB = lenB;
	}
	
}