/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class Sedan extends Car{
    private final double length;

    public Sedan(double length, double speed, double price, String color) {
        super(speed, price, color);
        this.length = length;
    }
    
    @Override
    public double getSalePrice() {
        if(length < 4500.0)
            return getPrice() - getPrice() * 0.1;
        else return getPrice();
    }
}
