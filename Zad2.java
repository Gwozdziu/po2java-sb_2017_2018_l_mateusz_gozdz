public class Zad2 {


	public static void main(String[] args) {
		if(args.length < 3) {
			System.out.println("Za mala liczba argumentow");
			System.exit(0);
		}
		try {
			
			int startIndex = Integer.parseInt(args[1]);
			int endIndex = Integer.parseInt(args[2]);
			System.out.println(args[0].substring(startIndex, endIndex + 1));
		} 
		
		catch(NumberFormatException | StringIndexOutOfBoundsException e) {
			System.out.println("Blad: " + e.getMessage());
		} 
	}
}
