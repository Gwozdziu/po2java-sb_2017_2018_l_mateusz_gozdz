/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class Truck extends Car{
    private final double weight;

    public Truck(double weight, double speed, double price, String color) {
        super(speed, price, color);
        this.weight = weight;
    }
    
    
    @Override
    public double getSalePrice() {
        if(weight > 3000.0)
            return getPrice() - getPrice() * 0.1;
        else return getPrice();
    }
}
