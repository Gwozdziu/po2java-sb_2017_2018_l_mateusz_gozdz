
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mateusz
 */
public class Zad1 {
    public static void main(String[] args){
        
        Game newGame = new Game();
        Scanner read = new Scanner(System.in);

        do
        {
            newGame.nextGame();
            System.out.println("Nowa gra rozpoczęta");
            while(newGame.checkWin(read.nextInt()));
            System.out.println("Jeśli chcesz zagrać ponownie wpisz \"tak\" w przeciwnym wypadku wyjdziesz z gry");
        }while(read.next().equalsIgnoreCase("tak"));
		read.close();
	}
}
class Game{
    private int winningNumber;
    private final Random random;
    private int tries;

    public Game() {
        random = new Random();
    }
    public boolean checkWin(int playersNumber){
        if(this.winningNumber > playersNumber)
        {
            System.out.println("Wylosowana liczba jest większa");
            tries++;
            return true;
        }
        else if(this.winningNumber < playersNumber)
        {
            System.out.println("Wylosowana liczba jest mniejsza");
            tries++;
            return true;
        }
        
        System.out.println("Wygrana! Liczba prób: " + tries);
        return false;
    }
    public void nextGame(){
        this.winningNumber = random.nextInt(100);
        this.tries = 0;
    } 
}
